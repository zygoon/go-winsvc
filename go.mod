module gitlab.com/zygoon/go-winsvc

go 1.21

require (
	gitlab.com/zygoon/go-cmdr v1.9.0
	golang.org/x/sys v0.14.0
)
