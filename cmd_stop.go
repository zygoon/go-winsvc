// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"context"
	"flag"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"golang.org/x/sys/windows/svc"
)

// stopCmd is a command for stopping a running service.
type stopCmd struct {
	serviceName string
}

// OneLiner provides a help message to the command router.
func (cmd *stopCmd) OneLiner() string {
	return "ask a running service to stop"
}

// Run sends the svc.Stop command to a service.
func (cmd *stopCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (cmd *stopCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmd *stopCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	if fl.NArg() != 0 {
		return errUnexpectedArg
	}

	return serviceCtl(cmd.serviceName, svc.Stop, svc.Stopped)
}
