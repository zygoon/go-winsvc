// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"golang.org/x/sys/windows/svc"
)

// continueCmd is a command for resuming execution of a paused service.
type continueCmd struct {
	serviceName string
}

// OneLiner provides a help message to the command router.
func (cmd *continueCmd) OneLiner() string {
	return "ask a paused service to continue"
}

// Run sends the svc.Continue command to a service.
func (cmd *continueCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (cmd *continueCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmd *continueCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	if fl.NArg() != 0 {
		return errUnexpectedArg
	}

	if err := serviceCtl(cmd.serviceName, svc.Continue, svc.Running); err != nil {
		return fmt.Errorf("cannot continue service: %w", err)
	}

	return nil
}
