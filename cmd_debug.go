// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"context"
	"flag"
	"sync"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/debug"
)

// debugCmd is a command for debugging the service interactively in a terminal.
type debugCmd struct {
	serviceName string
	serviceCmd  cmdr.Cmd
}

// OneLiner provides a help message to the command router.
func (*debugCmd) OneLiner() string {
	return "debug the service inside a terminal"
}

// Run starts the service interactively.
//
// When the cancelled the service is asked to stop. The command blocks
// until the command handler function returns.
func (cmd *debugCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (cmd *debugCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmd *debugCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	l := debug.New(cmd.serviceName)
	defer func() {
		_ = l.Close()
	}()

	r := make(chan svc.ChangeRequest)
	defer close(r)

	s := make(chan svc.Status)
	defer func() {
		// In the normal return path, s is closed explicitly below.
		if s != nil {
			close(s)
		}
	}()

	var wg sync.WaitGroup

	wg.Add(1)

	// Allow us to cancel the context. This is needed in case the function
	// terminates without the context being cancelled externally.
	ctx, cancel := context.WithCancel(ctx)

	go func() {
		defer wg.Done()

		status := svc.Status{State: svc.Stopped}
	loop:
		for {
			select {
			case <-ctx.Done():
				// When the context is cancelled, ask the service to stop.
				select {
				case r <- svc.ChangeRequest{Cmd: svc.Stop, CurrentStatus: status}:
				default:
					// The service cannot accept the configuration change.
					break
				}

				break loop
			case status = <-s:
				// Consume service status changes.
			}
		}

		// Consume service status changes until the serviceCmd.Run returns and
		// the channel is closed.
		for range s {
		}
	}()

	err := cmd.serviceCmd.Run(WithEnv(ctx, &Environment{R: r, S: s, L: l}), fl.Args())

	// The command stopped executing so we can safely close the status update
	// channel.
	close(s)

	// Cancel the context. This ensures that the go-routine terminates.
	cancel()

	wg.Wait()

	// Once the go-routine terminates, mark s as nil. This way the recovery
	// cleanup does not attempt to repeat the operation. This cannot be done
	// earlier without a data race.
	s = nil

	return err
}
