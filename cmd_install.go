// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"golang.org/x/sys/windows/svc/eventlog"
	"golang.org/x/sys/windows/svc/mgr"
)

// installCmd is a command for installing a service into the system registry.
type installCmd struct {
	serviceName string
	cfg         mgr.Config
}

// OneLiner provides a help message to the command router.
func (*installCmd) OneLiner() string {
	return "install the service to system registry"
}

// Run installs a service to the service registry.
//
// Arguments passed to the command are provided as arguments to the
// registered service executable.
func (cmd *installCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (cmd *installCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmd *installCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	path, err := exePath()
	if err != nil {
		return err
	}

	m, err := mgr.Connect()
	if err != nil {
		return err
	}

	defer func() {
		_ = m.Disconnect()
	}()

	name := cmd.serviceName

	s, err := m.OpenService(name)
	if err == nil {
		_ = s.Close()
		return fmt.Errorf("service %s already exists", name)
	}

	s, err = m.CreateService(name, path, cmd.cfg, fl.Args()...)
	if err != nil {
		return fmt.Errorf("cannot create service: %w", err)
	}

	defer func() {
		_ = s.Close()
	}()

	const eventMask = eventlog.Error | eventlog.Warning | eventlog.Info

	if err := eventlog.InstallAsEventCreate(name, eventMask); err != nil {
		_ = s.Delete()

		return fmt.Errorf("cannot install event source: %w", err)
	}

	return nil
}

const dotExe = ".exe"

func exePath() (string, error) {
	prog := os.Args[0]

again:

	p, err := filepath.Abs(prog)

	if err != nil {
		return "", err
	}

	fi, err := os.Stat(p)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) && !strings.HasSuffix(prog, dotExe) {
			prog += dotExe
			goto again
		}

		return "", err
	}

	if fi.Mode().IsDir() {
		return "", fmt.Errorf("%s is directory", p)
	}

	return p, nil
}
