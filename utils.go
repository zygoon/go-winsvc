// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
//go:build windows
// +build windows

package winsvc

import (
	"fmt"
	"time"

	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/mgr"
)

// serviceCtl sends a command to a service and waits for it to reach the desired state.
//
// The operation fails if the desired state is not reached within 10 seconds.
func serviceCtl(name string, c svc.Cmd, to svc.State) error {
	m, err := openMgr()
	if err != nil {
		return err
	}

	defer func() {
		_ = m.Disconnect()
	}()

	s, err := openSvc(m, name)
	if err != nil {
		return err
	}

	defer func() {
		_ = s.Close()
	}()

	status, err := s.Control(c)
	if err != nil {
		return fmt.Errorf("cannot send service control command %d: %w", c, err)
	}

	timeout := time.Now().Add(10 * time.Second)

	for status.State != to {
		if timeout.Before(time.Now()) {
			return fmt.Errorf("timeout waiting for service to go to state=%d", to)
		}

		time.Sleep(300 * time.Millisecond)

		status, err = s.Query()
		if err != nil {
			return fmt.Errorf("cannot retrieve service status: %w", err)
		}
	}

	return nil
}

func openMgr() (*mgr.Mgr, error) {
	m, err := mgr.Connect()
	if err != nil {
		return nil, fmt.Errorf("cannot connect to service manager: %w", err)
	}

	return m, nil
}

func openSvc(m *mgr.Mgr, name string) (*mgr.Service, error) {
	s, err := m.OpenService(name)
	if err != nil {
		return nil, fmt.Errorf("cannot access service: %w", err)
	}

	return s, nil
}
