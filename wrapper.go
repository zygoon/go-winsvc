// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"context"
	"errors"
	"log"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"
	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/eventlog"
	"golang.org/x/sys/windows/svc/mgr"
)

var errUnexpectedArg = errors.New("unexpected argument")

// Wrapper allows running commands as Windows services.
//
// The wrapper offers a set of standard sub-commands for installing, removing,
// pausing, continuing and debugging a Windows service. The wrapped command must
// use Env to retrieve service command and control channels from the context.
//
// While any command may be wrapped and ran as a service, commands should
// actively drain the command channel to behave properly.
type Wrapper struct {
	ServiceName   string
	ServiceCmd    cmdr.Cmd
	InstallConfig mgr.Config

	// ManagementFlags controls which management commands are enabled.
	//
	// The zero value offers backwards compatibility where all commands are
	// enabled. To enable a subset use SubsetMode together with a mask
	// representing the desired commands.
	ManagementFlags ManagementFlags
}

// ManagementFlags controls which of the management commands are enabled.
type ManagementFlags uint32

const (
	// SubsetMode indicates that not all management commands should be enabled.
	// When SubsetMode is not enabled then all remaining flags are ignored and
	// all commands are enabled automatically.
	SubsetMode ManagementFlags = 1 << iota
	// PauseAndContinue indicates that "pause" and "continue" commands should be
	// enabled.
	//
	// This only makes sense if the service is able to respond to the
	// corresponding commands and advertises support through the accept mask.
	PauseAndContinue
	// InstallAndRemove indicates that "install" and "remove" commands should be
	// enabled.
	InstallAndRemove
	// StartAndStop indicates that "start" and "stop" commands should be
	// enabled.
	StartAndStop
	// InteractiveDebug indicates that the "debug" command should be enabled.
	InteractiveDebug
)

// Run starts a Windows service or routes execution to management commands.
//
// Depending on the execution environment, one of two things happens. When Run
// is called from a Windows service process, it runs the ServiceCmd command
// directly. When running outside of a Windows service, for example from an
// interactive terminal, Run offers a set of management commands.
//
// The command can return any error. Errors posing as ServiceError can be used
// to convey service specific error codes.
func (cmd *Wrapper) Run(ctx context.Context, args []string) error {
	inService, err := svc.IsWindowsService()
	if err != nil {
		log.Fatalf("cannot determine is running as a service: %v\n", err)
	}

	if inService {
		// When running as a Windows service then run the service command
		// directly, skipping the convenience router below.
		return svc.Run(cmd.ServiceName, &serviceHandler{
			ctx:         ctx,
			serviceName: cmd.ServiceName,
			serviceCmd:  cmd.ServiceCmd,
		})
	}

	r := router.Router{Name: cmd.ServiceName, Commands: make(map[string]cmdr.Cmd)}

	// When SubsetMode is set enable the subset of configured commands. When
	// SubsetMode is not set enable all the commands for backwards
	// compatibility.

	if cmd.ManagementFlags&SubsetMode == 0 || cmd.ManagementFlags&InteractiveDebug == InteractiveDebug {
		r.Commands["debug"] = &debugCmd{serviceName: cmd.ServiceName, serviceCmd: cmd.ServiceCmd}
	}

	if cmd.ManagementFlags&SubsetMode == 0 || cmd.ManagementFlags&InstallAndRemove == InstallAndRemove {
		r.Commands["install"] = &installCmd{serviceName: cmd.ServiceName, cfg: cmd.InstallConfig}
		r.Commands["remove"] = &removeCmd{serviceName: cmd.ServiceName}
	}

	if cmd.ManagementFlags&SubsetMode == 0 || cmd.ManagementFlags&StartAndStop == StartAndStop {
		r.Commands["start"] = &startCmd{serviceName: cmd.ServiceName}
		r.Commands["stop"] = &stopCmd{serviceName: cmd.ServiceName}
	}

	if cmd.ManagementFlags&SubsetMode == 0 || cmd.ManagementFlags&PauseAndContinue == PauseAndContinue {
		r.Commands["pause"] = &pauseCmd{serviceName: cmd.ServiceName}
		r.Commands["continue"] = &continueCmd{serviceName: cmd.ServiceName}
	}

	return r.Run(ctx, args)
}

// serviceHandler implements scv.Handler and holds the context provided to ServiceWrapper.Run.
type serviceHandler struct {
	ctx         context.Context
	serviceName string
	serviceCmd  cmdr.Cmd
}

// Execute implements svc.Handler interface.
//
// Execute opens an event log and provides the service Environment through the
// context. The invoked ServiceCmd must call Env to retrieve it.
func (h *serviceHandler) Execute(args []string, r <-chan svc.ChangeRequest, s chan<- svc.Status) (bool, uint32) {
	l, err := eventlog.Open(h.serviceName)
	if err != nil {
		log.Fatalf("cannot open event log for service %s: %v\n", h.serviceName, err)
	}

	defer func() {
		_ = l.Close()
	}()

	env := &Environment{R: r, S: s, L: l}

	return ServiceReturn(h.serviceCmd.Run(WithEnv(h.ctx, env), args))
}
