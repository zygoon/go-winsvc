<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# About the Go Windows Service module

This module makes it easier to implement Windows services in Go. It is based on
the example service from https://golang.org/x/sys/windows/svc but unlike that
example it is meant to be used as a library.

The module depends on https://gitlab.com/zygoon/go-cmdr and is in fact
organized to allow nearly every `cmdr.Cmd` to run as a service. Proper services
must respond to configuration commands, but writing one is much easier this
way.

## Unstable Module Warning

This module is not yet mature enough to offer API stability.

For more information about Go modules and API stability see
https://go.dev/doc/modules/release-workflow#first-unstable

The module will be subsequently updated as the author gains experience with
Windows services and attempts to avoid problematic API. This most notably
affects the logger which does not integrate with services that write to
standard IO streams.

## Overview of the package

The entry point is the `Wrapper` type. It adapts an arbitrary function which
takes a context and a slice of arguments to run as a service. Minimalistic
configuration is required: the service must be given a system-wide unique name
and configuration provided to the service manager.

`Wrapper` implements the `cmdr.Cmd` interface and can be passed to
`cmdr.RunMain` inside the `main` function to set everything up. Wrapper offers a
number of commands which are discussed below. Use `Wrapper.ManagementFlags` to
tweak command selection if desired.

`Env` when given a `context.Context` returns the `Environment` which conveys
service-specific communication channels as well as access to the event log.
`Error` is an error capable of providing service-service specific return types
from a regular `cmdr.Cmd`. In general `ServiceReturn` provides such values from
any error.  Both `Environment` and `Error` are meant to adapt the `svc.Handler`
interface to the `cmdr.Cmd` interface.

### Wrapper commands

When invoked outside of a Windows service, the wrapper offers the following
sub-commands.

The `install` and `remove` commands require elevated permissions and allow to
register the service to be managed by Windows. Any arguments passed to
`install` are automatically provided to the service on startup.

The `start` and `stop` commands start and stop a service that was previously
registered with `install`. When `start` is called, the service process start to
execute. When `stop` is called the process (eventually) terminates.

The next set of commands offer particular interactions unique to the Windows
service system. The `pause` command tells a running service to pause whatever
the service may be doing. This does not cause the service process to terminate.
The `continue` command asks the service to continue paused execution.

Lastly the `debug` command allows a service to run interactively in a terminal
window. Windows services are not normally run this way but this is useful for
development purposes.

## Running commands as services

As mentioned, the intent of this module is to run any `cmdr.Cmd` as a service.
Practical services need some additional code.

Go documentation contains an example service which "beeps" periodically. The
following example shows how such a beeper, here named _pinger_, might look like.

The important novelty is that service-specific arguments are provided through
the command context. The winsvc.Env function returns an `Environment` object
with three fields.

There are two channels: `Environment.S` is a channel where the service can send
status updates. `Environment.R` is a channel where the service must receive
configuration commands. See golang.org/x/sys/windows/svc.Handler for details
about how those channels are used.

Lastly, there is a `Environment.L` which is a windows service logger. The
logger can log messages later available in the Windows Event Viewer.

```

func pinger(ctx context.Context, args []string) error {
	ev, _ := winsvc.Env(ctx)
	if ev == nil {
		return fmt.Errorf("pinger must run as a Windows service")
	}

	const acceptMask = svc.AcceptStop | svc.AcceptShutdown | svc.AcceptPauseAndContinue

	ev.S <- svc.Status{State: svc.StartPending}

	fasttick := time.Tick(1 * time.Second)
	slowtick := time.Tick(5 * time.Second)
	tick := fasttick

	ev.S <- svc.Status{State: svc.Running, Accepts: acceptMask}

loop:
	for {
		select {
		case <-tick:
			ev.L.Info(1, "ping")

		case c := <-ev.R:
			ev.L.Info(1, fmt.Sprintf("received service command: %#v", c))
			switch c.Cmd {
			case svc.Interrogate:
				ev.S <- c.CurrentStatus

			case svc.Stop, svc.Shutdown:
				break loop

			case svc.Pause:
				ev.S <- svc.Status{State: svc.Paused, Accepts: acceptMask}
				tick = slowtick

			case svc.Continue:
				ev.S <- svc.Status{State: svc.Running, Accepts: acceptMask}
				tick = fasttick

			default:
				ev.L.Error(1, fmt.Sprintf("unexpected control request #%v", c))
			}
		}
	}

	ev.S <- svc.Status{State: svc.StopPending}

	return nil
}
```

To run the pinger as a service all you need is to instantiate and run the
wrapper command.

```
func main() {
	cmd := winsvc.Wrapper{
		ServiceName: "pinger",
		ServiceCmd:  cmdr.Func(pinger),
		InstallConfig: mgr.Config{
			DisplayName: "Pinger demo service",
		},
	}

	cmdr.RunMain(&cmd, os.Args)
}
```

## Contributions

Contributions are welcome. Please try to respect Go requirements (1.16 at the
moment) and the overall coding and testing style.

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making
it easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
