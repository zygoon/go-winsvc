// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
)

// startCmd is a command for starting a stopped service.
type startCmd struct {
	serviceName string
}

// OneLiner provides a help message to the command router.
func (cmd *startCmd) OneLiner() string {
	return "ask a stopped service to start"
}

// Run asks the service manager to start the service.
func (cmd *startCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (cmd *startCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmd *startCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	m, err := openMgr()
	if err != nil {
		return err
	}

	defer func() {
		_ = m.Disconnect()
	}()

	s, err := openSvc(m, cmd.serviceName)
	if err != nil {
		return err
	}

	defer func() {
		_ = s.Close()
	}()

	if err := s.Start(fl.Args()...); err != nil {
		return fmt.Errorf("cannot start service: %w", err)
	}

	return nil
}
