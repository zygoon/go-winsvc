// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"golang.org/x/sys/windows/svc"
)

// pauseCmd is a command for pausing execution of a running service.
type pauseCmd struct {
	serviceName string
}

// OneLiner provides a help message to the command router.
func (cmd *pauseCmd) OneLiner() string {
	return "ask a running service to pause"
}

// Run sends the svc.Pause command to a service.
func (cmd *pauseCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (cmd *pauseCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmd *pauseCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	if fl.NArg() != 0 {
		return errUnexpectedArg
	}

	if err := serviceCtl(cmd.serviceName, svc.Pause, svc.Paused); err != nil {
		return fmt.Errorf("cannot pause service: %w", err)
	}

	return nil
}
