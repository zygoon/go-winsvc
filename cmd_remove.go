// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"golang.org/x/sys/windows/svc/eventlog"
)

// removeCmd is a command for removing a service from the system registry.
type removeCmd struct {
	serviceName string
}

// OneLiner provides a help message to the command router.
func (cmd *removeCmd) OneLiner() string {
	return "remove the service from system registry"
}

// Run removes an installed service from the system registry.
func (cmd *removeCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (cmd *removeCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (cmd *removeCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	if fl.NArg() != 0 {
		return errUnexpectedArg
	}

	m, err := openMgr()
	if err != nil {
		return err
	}

	defer func() {
		_ = m.Disconnect()
	}()

	s, err := openSvc(m, cmd.serviceName)
	if err != nil {
		return err
	}

	defer func() {
		_ = s.Close()
	}()

	if err := s.Delete(); err != nil {
		return fmt.Errorf("cannot delete service: %w", err)
	}

	if err := eventlog.Remove(cmd.serviceName); err != nil {
		return fmt.Errorf("cannot remove event source: %w", err)
	}

	return nil
}
