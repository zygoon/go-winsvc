// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"context"

	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/debug"
)

// Environment contains windows service command and control channels.
type Environment struct {
	// R is the channel where the system sends service change requests.
	R <-chan svc.ChangeRequest
	// C is the channel where the service can send updated status.
	S chan<- svc.Status
	// L is an interface for logging from Windows services. L is either a
	// debug.ConsoleLog or eventlog.Log depending on how the service is started.
	L debug.Log
}

type envKey struct{}

// WithEnv returns a context with a windows service environment.
func WithEnv(parent context.Context, ev *Environment) context.Context {
	return context.WithValue(parent, envKey{}, ev)
}

// Env retrieves a windows service environment from a context.
func Env(ctx context.Context) (*Environment, bool) {
	w, ok := ctx.Value(envKey{}).(*Environment)

	return w, ok
}
