// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package winsvc

import (
	"errors"
	"fmt"
	"syscall"
)

// Error conveys windows service from a cmdr.Cmd error return.
type Error struct {
	IsServiceSpecific bool
	ExitCode          uint32
}

// Error returns a message describing the error.
func (e *Error) Error() string {
	if e.IsServiceSpecific {
		return fmt.Sprintf("service-specific error code: %d", e.ExitCode)
	}

	return syscall.Errno(e.ExitCode).Error()
}

// ServiceReturn computes service exit code for a given error.
//
// Error instances carry both values directly. Any instances of syscall.Errno
// are treated as non-service-specific errors. All other non-nil errors return
// service-specific exit code 1.
func ServiceReturn(err error) (isServiceSpecific bool, code uint32) {
	var serviceErr *Error

	if errors.As(err, &serviceErr) {
		return serviceErr.IsServiceSpecific, serviceErr.ExitCode
	}

	var errnoErr syscall.Errno

	if errors.As(err, &errnoErr) {
		return false, uint32(errnoErr)
	}

	if err != nil {
		return true, 1
	}

	return false, 0
}
